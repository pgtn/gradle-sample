import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
 
import org.junit.Test;
import sample.Hello;
 
public class SampleTest {
 
    @Test
    public void test() {
        Hello hello = new Hello();
        assertThat(hello.getMessage(), is("Sample!"));
    }
}
